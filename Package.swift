// swift-tools-version:5.1
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "APIManager",
    platforms: [.iOS(.v12)],
    // platforms: [.iOS("9.0"), .macOS("10.10"), tvOS("9.0"), .watchOS("2.0")],
    products: [
        .library(name: "APIManager", targets: ["APIManager"])
    ],
    targets: [
        .target(
            name: "APIManager"
        )
    ]
)


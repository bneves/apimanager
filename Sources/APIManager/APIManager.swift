//Created by Bruno Neves
//www.brunoneves.com

import Foundation

public enum HTTPMethod: String {
    case options            = "OPTIONS"
    case get                = "GET"
    case head               = "HEAD"
    case post               = "POST"
    case put                = "PUT"
    case patch              = "PATCH"
    case delete             = "DELETE"
    case trace              = "TRACE"
    case connect            = "CONNECT"
}

public enum  HTTPStatusCode: Int, Error {
        case ok                  = 200
        case created             = 201
        case unauthorized        = 401
        case forbidden           = 403
        case notFound            = 404
        case methodNotAllowed    = 405
        case internalServerError = 500
}

extension Encodable { func toJSONData() -> Data? { return try? JSONEncoder().encode(self)}}

public class APIManager{
    
    public static var shared = APIManager()
    
    public func request<D:Decodable>(
        model:D.Type,
        url:String,
        method:HTTPMethod = .get,
        sync:Bool = false,
        token:String? = nil,
        body:Encodable? = nil,
        completionHandler:@escaping (_ obj:Decodable,_ status:HTTPStatusCode?, _ sucess: Bool) -> Void)
    {
        
        guard let url = URL(string:url) else { return }
        
        let configuration = URLSessionConfiguration.default
        //https://developer.apple.com/documentation/foundation/urlsessionconfiguration/multipathservicetype/handover
        //configuration.multipathServiceType = .handover
        
        let session:URLSession = URLSession(configuration: configuration)
        
        var request = URLRequest(url: url)
        
        request.httpMethod = method.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        
        var headers = request.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = "application/json"
        
        if token != nil {
            print("--------------")
            request.setValue("Bearer \(token!)", forHTTPHeaderField: "Authorization")
            headers["Authorization"] = "Bearer \(token!)"
            headers["X-CMC_PRO_API_KEY"] = "\(token!)"
        }
        
        request.allHTTPHeaderFields = headers
        
        if body != nil {
            do {
                request.httpBody =  body!.toJSONData()!
                //print("jsonData: ", String(data: request.httpBody!, encoding: .utf8) ?? "no body data")
            }
        }
        
        let semaphore = DispatchSemaphore(value: 0)
        
        DispatchQueue.global().async {
            
            session.dataTask(with: request) { (data, response, err) in
                
                guard let httpResponse = response as? HTTPURLResponse else { return }
                guard let data = data else { return }
                
                do {
                    
                    let object:Decodable = try JSONDecoder().decode(model.self, from: data)
                    
                    completionHandler(object as Decodable, HTTPStatusCode(rawValue: httpResponse.statusCode), true)
                    
                    if(sync){semaphore.signal()}
                    
                } catch let jsonErr {
                    
                    completionHandler(["error":"\(jsonErr)"] as Decodable, HTTPStatusCode(rawValue: httpResponse.statusCode), false)
                    
                    return
                    
                }
                
                }.resume()
        }
        if(sync){semaphore.wait(timeout: .distantFuture)}
        return
    }
    
}

import XCTest

import APIManagerTests

var tests = [XCTestCaseEntry]()
tests += APIManagerTests.allTests()
XCTMain(tests)

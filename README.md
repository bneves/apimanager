OBS: I'm still working on this project!

# APIManager
### A simple way to work with HTTP requests and JSON

---

To use it, simply:

import **APIManager.swift**

```js

Using NewsAPI

struct NewsResponse: Codable {
    let articles: [Article]
}

struct Article: Codable {
    let title: String
    let description: String?
}


let key = "YOUR_KEY"
let api = "https://newsapi.org/v2/top-headlines?country=ca&category=politics&apiKey=\(key)"

ApiManager.shared.request(
    model: NewsResponse.self,
    url: api
){ (obj:Decodable?,status:HTTPStatusCode?,sucess:Bool) in

    if sucess {
        let news:NewsResponse = obj as! NewsResponse
        print("Post: \(news.articles)")
    }else{
        print("Error: \(obj)")
    }
}


```

```js

How to POST

Endpoint
 "https://whereismyapiuri.herokuapp.com/api/tarot"

//JSON to send in the httpBody
{
    "card":"wheel_of_fortune",
    "is_reversed":0
}
//Response
{
    "tarot_reading": {
        "card_title":"Card Title",
        "card_meanings":"Bla Bla Bla Bla Bla Bla..."
    }
}
```
Ex:
```js
//Model
struct TarotDeck: Decodable { 
    let tarot_reading:Card? 
}
struct Card: Decodable {
    let card_title:String?
    let card_meanings:String?
}
//Post data
struct PostCard: Encodable {
    var card:String
    var is_reversed:Int
}

//Post
ApiManager.shared.request(
    model: TarotDeck.self,
    url: "https://whereismyapiuri.herokuapp.com/api/tarot",
    method: .post,
    sync:true,
    body: PostCard(
             card: "wheel_of_fortune",
             is_reversed: 0
          )
){ (obj:Decodable?,sucess:Bool) in
        if sucess {
            let tarot:TarotDeck = obj as! TarotDeck
            let reading = tarot.tarot_reading!

            print("1 - Card: \(reading.card_title!)")
            print("1 - Meaning: \(reading.card_meanings!)")
        }else {
            print("1 - error")
        }
}
```

```js

How to Get

Endpoint
 "https://jsonplaceholder.typicode.com/todos/22"
 
//JSON
{
    userId: 2,
    id: 22,
    title: "distinctio vitae autem nihil ut molestias quo",
    completed: true
}
```
```js
//Model
struct TestGet:Decodable{
    let userId: Int?
    let id: Int?
    let title: String?
    let completed: Bool?
}

//Get
ApiManager.shared.request(
    model: TestGet.self,
    url: "https://jsonplaceholder.typicode.com/todos/22"
){ (obj:Decodable?,sucess:Bool) in
    if sucess {
        print("2 - \(obj)")
    }else{
        print("2 - error")
    }
}

```

---

Bruno Neves **[www.brunoneves.com](http://www.brunoneves.com)**.
